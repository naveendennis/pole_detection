"""This module is the RESTful service entry point."""
from flask import Flask, Blueprint
from flask_cors import CORS

import os
import logging.config
import sys
from API import api
from API import ns_detect
from settings import get_value

logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), './logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)

app = Flask(__name__)
CORS(app)


def configure_app(flask_app):
    """Sets the Flask App's base configurations"""
    settings_to_apply = [
        'FLASK_SERVER_NAME',
        'SWAGGER_UI_DOC_EXPANSION',
        'RESTPLUS_VALIDATE',
        'RESTPLUS_MASK_SWAGGER',
        'ERROR_404_HELP'
    ]
    for key in settings_to_apply:
        flask_app.config[key] = get_value(key)


def initialize_app(flask_app):
    """Initializes and registers app"""
    configure_app(flask_app)

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(ns_detect)
    flask_app.register_blueprint(blueprint)


def run():
    print('starting')
    initialize_app(app)
    log.info('Starting development server at http://{}/api <<<<<'.format(
        app.config['FLASK_SERVER_NAME'])
    )
    app.run(port=get_value('FLASK_PORT'),
            host=get_value('FLASK_HOST'),
            debug=get_value('FLASK_DEBUG'))


if __name__ == '__main__':
    run()
