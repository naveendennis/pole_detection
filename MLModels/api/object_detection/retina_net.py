import keras
from sys import stdout
import numpy as np
from settings import get_value
# import keras_retinanet
import json
import cv2
from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
# from keras_retinanet.utils.gpu import setup_gpu


class RetinanetModel:

    model = None

    def __init__(self, image):
        super().__init__()
        self.image = image
        self.model = RetinanetModel.get_model()
        self.image = image
        self.pre_process()

    @classmethod
    def get_model(cls):
        if RetinanetModel.model is None:
            model_path = get_value('object_detection_model')

            # load retinanet model
            RetinanetModel.model = models.load_model(model_path, backbone_name='resnet50')
        return RetinanetModel.model

    def pre_process(self):
        self.image = preprocess_image(self.image)
        self.image, self.scale = resize_image(self.image)

    def predict(self):
        # print(self.image)
        boxes, scores, labels = self.model.predict_on_batch(np.expand_dims(self.image, axis=0))
        #boxes = boxes.reshape(-1, 4)
        boxes /= self.scale
        return boxes, scores, labels


class ObjectDetection:

    def __init__(self, image):
        image = image.read()
        img = np.fromstring(image, np.uint8)
        self.image = cv2.imdecode(img, cv2.IMREAD_COLOR)

    def predict(self):
        boxes, scores, labels = RetinanetModel(self.image).predict()
        output = {'Status': "", 'parts': [], "bounding box": ""}
        labels_to_names = {0: 'leaning_pole', 1: 'downed_pole', 2: 'fuse', 3: 'recloser', -1: "None"}
        for each_box, each_score, each_label in zip(boxes[0], scores[0], labels[0]):
            if each_label == -1:
                continue
            if each_label in [0, 1]:
                output['Status'] = labels_to_names[each_label]
                output['poles bounding box'] = {
                        "xmin": str(each_box[0]),
                        "ymin": str(each_box[1]),
                        "xmax": str(each_box[2]),
                        "ymax": str(each_box[3]),
                    }
            else:
                l = {
                    "label": labels_to_names[each_label],
                    "parts bounding box": {
                        "xmin": str(each_box[0]),
                        "ymin": str(each_box[1]),
                        "xmax": str(each_box[2]),
                        "ymax": str(each_box[3]),
                    }
                }
                output['parts'].append(l)
        # print(output)
        return output
