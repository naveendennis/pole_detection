"""This module serves the API endpoints for Contracts Route Requests"""
from flask import request
from flask_restplus import Resource, abort
from http import HTTPStatus
from API.modules.pole_detect.schemas import parser
import requests


from API import api
from MLModels.api.object_detection import retina_net

import logging

log = logging.getLogger(__name__)


class Pole_Detect(Resource):

    def post(self):
        if 'Image' not in request.files:
            error_message = 'Missing File in Request'
            print('Image: ', error_message)
            return abort(400, error={'msg': error_message})

        # The Parsed incoming GET Request body object
        camera_req = parser.parse_args()
        Image = camera_req['Image']

        # Query model for rebates
        model = retina_net.ObjectDetection(Image)
        pred_output = model.predict()
        return pred_output, HTTPStatus.OK
