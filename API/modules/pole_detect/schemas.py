"""Module describes the marshal schemas for the WOPr Resource"""
from werkzeug.datastructures import FileStorage
from flask_restplus import reqparse


parser = reqparse.RequestParser()
parser.add_argument('Image', type=FileStorage, location='files')
