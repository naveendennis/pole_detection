"""Module that creates the Api object and declares default error handler."""
from flask_restplus import Api

# The main entry point for the application. Initialized in app.py
api = Api(version='1.0',
          title='Supplychain Contracts',
          description='',
          default='pole_detect',
          default_label='Supply Chain Contracts',
          catch_all_404s=True,
          ordered=True)
