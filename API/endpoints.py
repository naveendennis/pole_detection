"""This module serves the API endpoints."""
from API.restplus import api
from API.modules.pole_detect.resources import Pole_Detect


# Add Namespace for Contracts Endpoint
ns_detect = api.namespace(
    name='TeamGOAT',
    path='/teamgoat',
)

# Add Resource to ns_contracts Namespace
ns_detect.add_resource(Pole_Detect, '/pole_detect')
