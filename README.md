To Run:
./app.py

Postman:
GET /api/supplychain/contracts HTTP/1.1
Host: 127.0.0.1:8008
Content-Type: application/json
{
    "data": {**contract_data}
}


Postman:
import requests

url = "http://127.0.0.1:8008/api/supplychain/contracts"

payload = {"data": {**contract_data}}
headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache",
    'Authorization': "Bearer {**jwt}"
    }

response = requests.request("GET", url, data=payload, headers=headers)

print(response.text)

