"""Settings file."""
import os
from pathlib import Path


def get_value(key):
    """Returns a value from the corresponding env var or from settings if env var doesn't exist."""
    port = int(os.environ.get('PORT', 8080))
    host = str(os.environ.get('HOST', '0.0.0.0'))
    settings = {
        # Flask settings
        'FLASK_SERVER_NAME': '{host}:{port}'.format(host=host, port=port),
        'FLASK_HOST': host,
        'FLASK_PORT': port,
        'FLASK_DEBUG': False,  # ONLY LOCAL, NOT DEV OR PROD. Do not use debug mode in production.

        # Flask-Restplus settings
        'SWAGGER_UI_DOC_EXPANSION': 'list',
        'RESTPLUS_VALIDATE': True,
        'RESTPLUS_MASK_SWAGGER': False,
        'ERROR_404_HELP': True,
        'SWAGGER_UI_JSONEDITOR': True,


        # Model locations
        "object_detection_model": 'MLModels/models/optithon_base.h5',
        # Rate
        "pole_labels": ["down", "leaning"],
        "part_labels": ["recloser", "fuses_distribution_line", "utility_distribution_capacitor"]


    }
    return os.environ[key] if key in os.environ else settings[key]


def get_project_root() -> Path:
    """Returns project root folder"""
    return Path(__file__).parent
